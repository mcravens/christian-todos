## Useful commands (requires aws-cli, sam-cli, cdk-cli global install)

- `npm run build` compile typescript to js
- `npm run watch` watch for changes and compile
- `cdk diff` compare deployed stack with current state
- `cdk synth` emits the synthesized CloudFormation template - do this before deploying!
- `cdk deploy` deploy this stack to your default AWS account/region
