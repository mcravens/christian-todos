import * as cdk from "@aws-cdk/core";
import * as christianTodoService from "./christian-todo_service";

export class ChristianTodoStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    new christianTodoService.ChristianTodoService(this, "ChristianTodoService");
  }
}
