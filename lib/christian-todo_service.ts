import * as core from "@aws-cdk/core";
import * as apigateway from "@aws-cdk/aws-apigateway";
import * as lambda from "@aws-cdk/aws-lambda";
import * as dynamodb from "@aws-cdk/aws-dynamodb";
import * as s3 from "@aws-cdk/aws-s3";
import * as S3Deployment from "@aws-cdk/aws-s3-deployment";

const path = "./files";

export class ChristianTodoService extends core.Construct {
  constructor(scope: core.Construct, id: string) {
    super(scope, id);

    const handler = new lambda.Function(this, "ApiHandler", {
      runtime: lambda.Runtime.NODEJS_14_X,
      code: lambda.Code.fromAsset("resources"),
      handler: "api.main",
    });

    const api = new apigateway.RestApi(this, "api", {
      restApiName: "Christian Todo Api",
      description: "This service serves christian's todo list api.",
      deployOptions: {
        throttlingRateLimit: 2,
        throttlingBurstLimit: 1,
      },
      defaultCorsPreflightOptions: {
        allowOrigins: apigateway.Cors.ALL_ORIGINS,
      },
    });

    const table = new dynamodb.Table(this, id, {
      tableName: "christian-todo",
      billingMode: dynamodb.BillingMode.PROVISIONED,
      readCapacity: 1,
      writeCapacity: 1,
      removalPolicy: core.RemovalPolicy.DESTROY,
      partitionKey: { name: "id", type: dynamodb.AttributeType.STRING },
      sortKey: { name: "createdAt", type: dynamodb.AttributeType.NUMBER },
      pointInTimeRecovery: true,
    });

    const bucket = new s3.Bucket(this, "ChristianTodoFrontendBucket", {
      autoDeleteObjects: true,
      removalPolicy: core.RemovalPolicy.DESTROY,
      websiteIndexDocument: "index.html",
      publicReadAccess: true,
    });
    new S3Deployment.BucketDeployment(this, "Deployment", {
      sources: [S3Deployment.Source.asset(path)],
      destinationBucket: bucket,
    });
    new core.CfnOutput(this, "BucketDomain", {
      value: bucket.bucketWebsiteDomainName,
    });

    table.grant(handler, "dynamodb:Scan", "dynamodb:PutItem");

    const todosIntegration = new apigateway.LambdaIntegration(handler, {
      requestTemplates: { "application/json": '{ "statusCode": "200" }' },
    });

    const todos = api.root.addResource("todos");

    todos.addMethod("GET", todosIntegration);
    todos.addMethod("POST", todosIntegration);
  }
}
